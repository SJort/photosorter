"""
Class used in the setup window to graphically map shortcuts to folders.
Represents one mapping.
"""
import tkinter as tk

from main.FolderPicker import FolderPicker
from main.ShortcutChooser import ShortcutChooser


class MappingBox:
    def __init__(self, root, delete_function, mapping, index):
        """
        :param root: The frame to place the box in.
        :param delete_function: The function to call if the delete button is pressed of this mapping.
        :param mapping: A reference to the mapping which is updated using this GUI element.
        :param index: The index which is saved to the metadata.
        """
        self.frame = tk.Frame(root)  # put all elements in one box
        self.delete_function = delete_function
        self.mapping = mapping
        self.index = index

    def draw_on_frame(self):
        """
        Separate function to draw the mapping box items on the frame,
        because they are all redrawn when one is added / deleted
        """

        column_index = 0  # set column index by code for easy adding / removing / changing order of column items

        # index indicator
        self.index_label = tk.Label(self.frame, text=f"{self.index} - ", font="Helvetica 10 bold")
        self.index_label.grid(row=0, column=column_index, sticky=tk.NSEW)
        column_index += 1


        # shortcut button
        self.shortcut_button = None  # button needs reference to itself, see definition
        # set button text to the shortcut if it is defined
        text = "Set" if self.mapping.shortcut == "" else self.mapping.shortcut
        self.shortcut_button = tk.Button(self.frame, text=text,
                                         command=lambda: ShortcutChooser(mapping=self.mapping,
                                                                         button=self.shortcut_button))
        self.shortcut_button.configure(width=3)
        self.shortcut_button.grid(row=0, column=column_index)
        column_index += 1

        # folder text field
        self.frame.grid_columnconfigure(column_index, weight=1)  # let field fill horizontal width
        self.folder_entry_sv = tk.StringVar()

        # update mapping on each text field edit
        def callback(sv):
            self.mapping.folder = sv.get()

        self.folder_entry_sv.trace("w", lambda name, index, mode, sv=self.folder_entry_sv: callback(sv))

        # set text of field equal to configured value
        self.folder_entry_sv.set(self.mapping.folder)

        self.entry = tk.Entry(self.frame, textvariable=self.folder_entry_sv)
        self.entry.grid(row=0, column=column_index, sticky=tk.NSEW)
        column_index += 1

        # choose folder button
        self.folder_button = tk.Button(self.frame, text="Choose",
                                       command=lambda: FolderPicker.pick(initial=self.mapping.folder,
                                                                         stringvar_to_update=self.folder_entry_sv,
                                                                         mapping_to_update=self.mapping))
        self.folder_button.grid(row=0, column=column_index)
        column_index += 1

        # delete button
        self.delete_button = tk.Button(self.frame, text="X",
                                       command=lambda: self.delete_function(self.mapping))
        self.delete_button.grid(row=0, column=column_index)
        column_index += 1
