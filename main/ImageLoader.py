"""
Class responsible for rendering images on a canvas and reading the image folder.
Loads the next X images into memory on a separate thread, making rendering faster.
Usage demonstration: see bottom

- set the path for it to start indexing
- resets the index to 0 when folder contents change
- reuses its thread
- if image is not loaded by the thread, you have to wait for it
"""
import os
import threading
import time
import tkinter as tk

from PIL import Image, ImageTk
from natsort import natsorted


class ImageItem:
    def __init__(self, path):
        self.path = path
        self.image = None  # a loaded image in memory with specified dimensions
        self.tkinter_image = None  # IMPORTANT: this reference needs to be kept for it to display on the canvas

    def get_filename(self):
        filename = os.path.basename(self.path)
        return str(filename)

    def load(self, width=300, height=300):
        # load an image to memory and resize it
        if self.is_loaded():
            return
        print(f"Loading {self} to memory.")
        img = Image.open(self.path)

        # downsize the image, keeping aspect ratio, either filling horizontally, vertically, or perfect fit
        # the resizing happens here in the load function, because it takes like 0.3 seconds
        width_percent = (width / float(img.size[0]))  # basically aspect ratio
        height_size = int((float(img.size[1]) * float(width_percent)))  # what the resized height should be
        if height_size > height:  # if resized height is not small enough, let image fill width
            height_percent = (height / float(img.size[1]))
            width_size = int((float(img.size[0]) * float(height_percent)))
            self.image = img.resize((width_size, height), Image.ANTIALIAS)
        else:  # resized height fits in requested dimensions, fill height
            self.image = img.resize((width, height_size), Image.ANTIALIAS)

    def unload(self):
        if not self.is_loaded():
            return
        print(f"Unloading {self} from memory.")
        self.image = None
        self.tkinter_image = None

    def is_loaded(self):
        return self.image is not None

    def render(self, canvas):
        """
        :param canvas: The tkinter canvas to create the image on. Canvas gets cleared.
        """
        if not self.is_loaded():
            print(f"Can't render unloaded image.")
            return

        print(f"Rendering {self}")
        try:
            canvas.delete("all")  # clear canvas, images otherwise stack
            max_width = canvas.winfo_width()
            max_height = canvas.winfo_height()
            # IMPORTANT: the reference needs to be kept in order to display on the canvas
            # it needs an existing tkinter instance for it to work, so its done here and not in load function
            self.tkinter_image = ImageTk.PhotoImage(image=self.image)
            canvas.create_image(max_width // 2, max_height // 2, anchor=tk.CENTER, image=self.tkinter_image)
        except Exception as e:
            print(f"Exception rendering image: {e}")
            # raise e

    def __repr__(self):
        return f"{{{self.__class__.__name__}: path='{self.path}'}}"

    def __eq__(self, other):
        if other is None:
            return False
        # only compare image paths, because it is unique for each image
        return self.path == other.path


class ImageLoader:
    def __init__(self):
        print(f"Initializing {self.__class__.__name__}")
        self.path = None
        self.current_image = None
        self.image_index = 0
        self.amount_to_forward_load = 40
        self.amount_to_backward_load = 10
        self.image_width = 400
        self.image_height = 300

        self.images = []
        self.main_thread = threading.Thread(target=self.main_thread_function)
        self.main_thread.setDaemon(True)
        self.main_thread.start()

    def set_path(self, path):
        """
        Set the path with images for the image loader to load to memory.
        Thread automatically reindexes when path is changed.
        This function can be used before any tkinter stuff is started.
        """
        self.path = path
        print(f"Set image loader path to {self.path}")

    def update_image_index(self, amount=1):
        self.image_index = (self.image_index + amount) % len(self.images)  # loop index around. google: circular list
        print(f"Set image index to {self.image_index}.")
        self.current_image = self.images[self.image_index]

    def get_surrounding_items(self, items):
        """
        Uses the image index and backward/forward values to find neighboring items in a list.
        Example: [a, b, c, d] -> index=0, back&forward=1 -> [a, b, d] because b and d are neighbors of a
        :param items: List of items to filter.
        :return: New list of surrounding items.
        """
        result_index_list = []  # list which is going to contain indexing ints of the neighboring items
        start = self.image_index - self.amount_to_backward_load  # most left neighbor, can be negative
        end = self.image_index + self.amount_to_forward_load + 1  # most right neighbor, can exceed list length
        for i in range(start, end):
            circled_index = i % len(self.images)  # loop index around list. google: circular list
            if circled_index not in result_index_list:  # don't allow duplicates in result
                result_index_list.append(circled_index)

        # return new list with only the neighboring items
        result_items = []
        for i in result_index_list:
            result_items.append(items[i])
        return result_items

    def unload_all(self):
        for image in self.images:
            image.unload()

    def main_thread_function(self):
        while True:
            try:
                # path is None at start, wait for it to be set
                if self.path is None:
                    # print(f"Waiting for path to be set.")
                    time.sleep(1)
                    continue

                # get all the images from folder and convert them to image objects
                # could have: the frequency this happens may be lowered
                filenames = os.listdir(self.path)
                paths = [os.path.join(self.path, filename) for filename in filenames]
                paths = natsorted(paths, key=lambda y: y.lower())  # sort items same as the file manager does
                read_images = [ImageItem(filename) for filename in paths if self.is_image(filename)]

                # if the current images in the folder do not match the last retrieved images, set index to 0
                # reason: images may be added to the left of current image, so user may forget to sort those
                # could have: give user the option to stay
                if self.images != read_images:
                    print(f"Items in folder have changed, setting index to 0 and unloading all images.")
                    self.image_index = 0
                    # unload all images (could already be done by garbage collector, but just to be sure)
                    for image in self.images:
                        image.unload()
                    self.images = read_images

                # set the current image (should only happen when folder is set, otherwise by set_index())
                if len(self.images) == 0:
                    print(f"Waiting for images to load.")
                    time.sleep(0.1)
                    continue
                if self.current_image != self.images[self.image_index]:  # todo: if statement may not be needed
                    print(f"Updating current image.")
                    self.current_image = self.images[self.image_index]

                # load current and neighboring images to memory, unloading others
                images_to_load = self.get_surrounding_items(self.images)  # list of images to be loaded to memory
                for image in self.images:
                    if image in images_to_load:
                        image.load(self.image_width, self.image_height)
                    else:
                        image.unload()

                time.sleep(0.1)
            except Exception as e:
                print(f"Exception in thread: {e}")
                raise e

    def is_image(self, filename):
        path = os.path.join(self.path, filename)
        if "." not in path:
            return False
        if not os.path.isfile(path):
            return False
        if path.split(".")[-1].lower() not in ["png", "jpg", "jpeg"]:
            return False
        return True


image_loader = ImageLoader()

if __name__ == "__main__":
    print(f"Started image loading test.")

    path = "/home/jort/this/images/sorting2/"
    image_loader.set_path(path)


    def on_key(event):
        pressed_key = str(event.char)
        print(f"Pressed {pressed_key}")

        if pressed_key == "a":
            image_loader.update_image_index(-1)
        elif pressed_key == "1":
            p = "/home/jort/this/images/sorting2/"
            image_loader.set_path(p)
        elif pressed_key == "2":
            p = "/home/jort/this/images/sorting/"
            image_loader.set_path(p)
        else:
            image_loader.update_image_index()

        if image_loader.current_image is None:
            print(f"Image none")
            return
        if not image_loader.current_image.is_loaded():
            print(f"Not loaded")
            return
        image_loader.current_image.render(canvas)


    root = tk.Tk()
    root.bind("<Escape>", lambda x: root.destroy())
    root.minsize(500, 400)
    root.bind("<Key>", on_key)
    canvas = tk.Canvas(root, bd=2, relief="solid")
    canvas.pack()
    root.mainloop()
