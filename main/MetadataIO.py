"""
!discontinued because of exif and metadata limitations, only seems to have proper support for jpegs

Sets attributes for certain images.
Writes it some EXIF value.

All existing EXIF tags: https://www.exiv2.org/tags.html
EXIF Python library: https://exif.readthedocs.io/en/latest/usage.html

Usage example: see bottom.

Notes:
- maybe add save option for copy or cut
- one reason this wrapper is created is for it to be possible to easily change the data saving method
- saving in exif data does not sound like a clean solution
- data may be lost when file is copied the wrong way (or that's only extended metadata attributes?)
"""

from exif import Image

from main.Timer import *


class MetadataIO:
    def __init__(self):
        self.index_attribute = "photographic_sensitivity"

    def set_index(self, image_path, index):
        start_timer("set_index")
        with open(image_path, "rb") as image_file:
            my_image = Image(image_file)
            my_image.set(self.index_attribute, str(index))

            with open(image_path, "wb") as new_image_file:
                new_image_file.write(my_image.get_file())
        print(f"Set index to {index} of {image_path} in {get_elapsed('set_index')}ms.")

    def get_index(self, image_path):
        start_timer("get_index")
        result = None
        with open(image_path, "rb") as image_file:
            my_image = Image(image_file)
            attributes = my_image.get_all()
            if self.index_attribute in attributes:
                result = my_image.get(self.index_attribute)
                result = int(result)

        print(f"Read index attribute {result} from {image_path} in {get_elapsed('get_index')}ms.")
        return result


metadata_io = MetadataIO()

if __name__ == "__main__":
    path = "../img/img1.JPG"
    metadata_io.set_index(path, 2)
    result = metadata_io.get_index(path)
    print(f"Read {result}")
