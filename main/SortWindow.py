import os.path
import threading
import time
import tkinter as tk

from main.ImageLoader import image_loader
from main.Logger import logger
from main.Settings import settings
from main.SortFile import sort_file
from main.Timer import *


class SortWindow:
    def __init__(self, frame):
        self.frame = frame

        self.frame.bind("<Key>", self.on_key)
        self.frame.bind('<Left>', lambda e: image_loader.update_image_index(-1) or self.show_current_image())
        self.frame.bind('<Right>', lambda e: image_loader.update_image_index(1) or self.show_current_image())

        self.main_thread = None

        # top information frame
        self.info_frame = tk.Frame(self.frame)
        self.frame.grid_rowconfigure(1, weight=1)
        self.frame.grid_columnconfigure(0, weight=1)
        self.info_frame.grid(row=0, column=0, padx=10, pady=10, sticky=tk.NSEW)

        # image index label (3/8)
        self.count_label_text = tk.StringVar()
        self.count_label = tk.Label(self.info_frame, textvariable=self.count_label_text)

        self.info_frame.grid_rowconfigure(0, weight=1)
        self.info_frame.grid_columnconfigure(2, weight=1)
        self.count_label.grid(row=0, column=2, sticky=tk.E, padx=10)

        # image name label (girl.jpg)
        self.filename_label_text = tk.StringVar()
        self.filename_label = tk.Label(self.info_frame, textvariable=self.filename_label_text)

        self.info_frame.grid_rowconfigure(0, weight=1)
        self.info_frame.grid_columnconfigure(1, weight=1)
        self.filename_label.grid(row=0, column=1)

        # advertisement
        self.text_label = tk.Label(self.info_frame, text="Jorts PhotoSorter")

        self.info_frame.grid_rowconfigure(0, weight=1)
        self.info_frame.grid_columnconfigure(0, weight=1)
        self.text_label.grid(row=0, column=0, sticky=tk.W, padx=10)

        # canvas below information frame taking all available space
        self.canvas = tk.Canvas(self.frame)
        self.canvas.bind("<Configure>", self.resize_canvas)
        self.frame.grid_rowconfigure(1, weight=1)
        self.frame.grid_columnconfigure(0, weight=1)
        self.canvas.grid(row=1, column=0, columnspan=3, padx=10, pady=10, sticky=tk.NSEW)
        self.c_w = self.canvas.winfo_width()
        self.c_h = self.canvas.winfo_height()

        # bottom frame with all the shortcut buttons
        self.mapping_buttons = []
        self.button_frame = tk.Frame(self.frame)  # bd=2, relief="solid"
        self.frame.grid_rowconfigure(2, weight=1)
        self.button_frame.grid(row=2, column=0, columnspan=3, padx=10, pady=10, sticky="esw")

        self.start_thread()

    def resize_canvas(self, event):
        print(f"Resizing canvas to {event.width}x{event.height}")
        image_loader.image_width = event.width
        image_loader.image_height = event.height

        # because the current image which gets unloaded, gets rendered by the thread when it is loaded again
        self.canvas.delete("all")

        image_loader.unload_all()
        self.show_current_image()

    def fill_button_frame(self):
        # fills the bottom row with button corresponding to the mappings, call this when sort window is opened
        print(f"Filling buton frame.")
        column_index = 0  # also the mapping index
        self.mapping_buttons.clear()
        for mapping in settings.get_mappings():
            button = tk.Button(master=self.button_frame,
                               text=f"'{mapping.shortcut}' -> {mapping.get_folder_name()}",
                               # lambda needs to capture the column index, otherwise it can change and always is the highest index
                               command=lambda index=column_index: self.sort_and_next_image(index),
                               bd=2, relief="solid")
            button.grid(row=0, column=column_index)
            self.button_frame.grid_columnconfigure(column_index, weight=1)  # centers and evenly spreads buttons
            self.mapping_buttons.append(button)
            column_index += 1

    def sort_and_next_image(self, index):
        filename = image_loader.current_image.get_filename()  # save path because current image changes
        self.color_button(index)
        image_loader.update_image_index()
        self.show_current_image()
        sort_file.set_index(filename, index)

    def color_button(self, index):
        for i in range(len(self.mapping_buttons)):
            button = self.mapping_buttons[i]
            if i == index:
                button.configure(background="forestgreen", fg="white")
            else:
                button.configure(background="lightgrey", fg="black")

    def show_current_image(self):
        if image_loader.current_image is None:
            print(f"Image is None.")
            return False
        if not image_loader.current_image.is_loaded():
            print(f"Image not loaded.")
            return False
        image_loader.current_image.render(self.canvas)
        self.color_button(sort_file.get_index(image_loader.current_image.get_filename()))
        return True

    def on_key(self, event):
        pressed_key = str(event.char)
        print(f"{get_elapsed('runtime')}: pressed {pressed_key}")

        # find the mapping of which shortcut matches the pressed key
        matching_mapping = None
        mapping_index = 0
        for mapping in settings.get_mappings():
            if pressed_key == mapping.shortcut:
                matching_mapping = mapping
                break
            mapping_index += 1

        # ignore and show alert if nothing matched
        if matching_mapping is None:
            logger.show_info(f"Pressed key '{pressed_key}' does not match the shortcut of any mapping.")
            return

        self.sort_and_next_image(mapping_index)

    def thread_function(self):
        while True:
            # update the label count (could also be done when index is changed
            text = f"Image {image_loader.image_index}/{len(image_loader.images)}"
            self.count_label_text.set(text)
            text = "Loading..."
            if image_loader.current_image is not None:
                text = f"{image_loader.current_image.get_filename()}"
                if not self.canvas.find_all():  # if canvas is empty, render the current image when it is loaded
                    if image_loader.current_image.is_loaded():
                        image_loader.current_image.render(self.canvas)
                        # button can't be colored earlier as current image is null
                        self.color_button(sort_file.get_index(image_loader.current_image.get_filename()))
            self.filename_label_text.set(text)
            time.sleep(0.05)

    def start_thread(self):
        self.main_thread = threading.Thread(target=self.thread_function, args=())
        self.main_thread.setDaemon(True)
        self.main_thread.start()
