"""
Very specific class to bind a single alphanumeric key to the shortcut attribute of an object.
In this case that is a Mapping object.
A button can also be passed, of which the text is updated to the typed key.
This works by a reference of those two objects,
since it launches a new tkinter window which is destroyed after correct input.

Example usage: see bottom
"""
import tkinter as tk

from main.Mapping import Mapping


class ShortcutChooser:

    def __init__(self, mapping, button=None):
        self.mapping = mapping
        self.button = button
        self.launch()

    def launch(self):
        print("Press a shortcut key.")
        self.root = tk.Tk()
        self.root.bind("<Key>", self.on_key)
        self.root.bind("<Escape>", lambda x: self.root.destroy())
        self.root.title("Key input")
        self.root.geometry("400x300")
        label = tk.Label(self.root, text="Press a key to use as shortcut")
        self.root.grid_rowconfigure(0, weight=1)
        self.root.grid_columnconfigure(0, weight=1)
        label.grid(row=0, column=0, sticky=tk.NSEW)

        text_end = "." if self.mapping.folder is None else f"'{self.mapping.get_folder_name()}'."
        text = f"This will be the key to press to move the image to folder\n {text_end}"
        label2 = tk.Label(self.root, text=text)
        self.root.grid_rowconfigure(0, weight=1)
        self.root.grid_columnconfigure(0, weight=1)
        label2.grid(row=1, column=0, sticky=tk.NSEW)
        button = tk.Button(self.root, text="Cancel", command=self.root.destroy)
        button.grid(row=3, column=0, sticky=tk.S)
        self.info_label = tk.Label(self.root, fg="red")
        self.info_label.grid(row=2, column=0)
        self.root.focus_force()
        self.root.mainloop()

    def on_key(self, event):
        pressed_key = str(event.char)
        # print(f"Pressed {pressed_key} {type(pressed_key)}")
        if not pressed_key.isalpha():
            print(f"Typed key '{pressed_key} is not alphanumeric, ignoring.")
            self.info_label["text"] = f"Not an alphanumeric key: '{pressed_key}'"
            return

        self.root.destroy()
        self.mapping.shortcut = pressed_key
        print(f"Configured {self.mapping.shortcut} as shortcut.")
        if self.button is not None:
            self.button.configure(text=self.mapping.shortcut)


if __name__ == "__main__":
    root = tk.Tk()
    root.bind("<Escape>", lambda x: root.destroy())
    mapping = Mapping()
    root.geometry("300x300")


    def launch_chooser():
        chooser = ShortcutChooser(mapping=mapping, button=button)
        chooser.launch()


    button = tk.Button(root, text="go", command=launch_chooser)
    button.pack()
    root.mainloop()
