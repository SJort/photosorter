"""
A class which links a alphanumeric key to a folder.
Convert and load from string possible.

Notes:
- also add index?
"""
from pathlib import Path


class Mapping:
    def __init__(self, folder="", shortcut=""):
        self.folder = folder
        self.shortcut = shortcut

    def __str__(self):
        return f"Mapping: '{self.shortcut}' to {self.folder}"

    def serialize(self):
        return f"{self.shortcut} , {self.folder}"

    @staticmethod
    def deserialize(serial):
        split = [x.strip() for x in serial.split(",")]
        shortcut = split[0]
        folder = split[1]
        return Mapping(shortcut=shortcut, folder=folder)

    def get_folder_name(self):
        if self.folder is None:
            return ""
        return Path(self.folder).stem

