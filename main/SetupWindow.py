import os
import tkinter as tk

from main.FolderPicker import FolderPicker
from main.Logger import logger
from main.Mapping import Mapping
from main.MappingBox import MappingBox
from main.Settings import Settings, settings


class SetupWindow:
    def __init__(self, frame, control):
        self.frame = frame

        # let children take all space horizontally (centers all items)
        self.frame.grid_columnconfigure(0, weight=1)

        # info
        self.label = tk.Label(self.frame, text="Setup window", font=("Arial", 22))
        self.label.grid(row=0, column=0)

        # source folder picker
        self.root_folder_frame = tk.Frame(self.frame)
        self.root_folder_frame.grid(row=1, column=0, sticky=tk.NSEW, padx=20)
        self.folder_entry_sv = tk.StringVar()
        self.setup_source_folder_picker()

        # mapping boxes
        self.frame.grid_rowconfigure(2, weight=1)
        self.mapping_box_frame = None

        # add mapping box button
        self.add_button = tk.Button(self.frame, text="Add", command=self.add_mapping)
        self.add_button.grid(row=3, column=0)

        # start button
        self.button = tk.Button(self.frame, text="Start sorting", command=control.load_sorting_window)
        self.button.grid(row=4, column=0)

        self.draw_mapping_boxes()

    def on_source_folder_edit(self):
        # function called to validate folder input
        print("Edited source folder to", self.folder_entry_sv.get())
        settings.source_folder = self.folder_entry_sv.get()
        return True  # accept all changes, otherwise its gets declined whilst typing
        # (validation can also happen when focus leaves text field)

    def setup_source_folder_picker(self):
        self.folder_entry_sv.set(settings.source_folder)  # set text of field equal to configured value

        self.root_folder_frame.grid_columnconfigure(0, weight=1)  # let field fill horizontal width
        entry = tk.Entry(self.root_folder_frame,
                         textvariable=self.folder_entry_sv,
                         validate="key",  # validate input on each change
                         validatecommand=self.on_source_folder_edit)
        entry.grid(row=0, column=0, sticky=tk.NSEW)

        # choose folder button
        folder_button = tk.Button(self.root_folder_frame, text="Choose",
                                  # the stringvar listener thing updates the settings in turn
                                  command=lambda: FolderPicker.pick(stringvar_to_update=self.folder_entry_sv,
                                                                    initial=self.folder_entry_sv.get()))
        folder_button.grid(row=0, column=1)

    def draw_mapping_boxes(self):
        print("Redrawing mapping boxes...")
        index = 0
        new_mapping_box_frame = tk.Frame(self.frame)  # an attempt to prevent the flickering...

        for mapping in settings.get_mappings():
            print(f"Drawing mapping box with index {index}")
            new_mapping_box = MappingBox(root=new_mapping_box_frame, delete_function=self.remove_mapping,
                                         mapping=mapping, index=index)

            # children take all horizontal space (center horizontally)
            new_mapping_box_frame.grid_columnconfigure(0, weight=1)

            new_mapping_box.draw_on_frame()
            new_mapping_box.frame.grid(row=index, column=0, sticky=tk.NSEW, padx=20)
            index += 1

        if self.mapping_box_frame is not None:
            self.mapping_box_frame.destroy()
        # let this row take all available space vertically
        new_mapping_box_frame.grid(row=2, column=0, sticky=tk.NSEW)
        self.mapping_box_frame = new_mapping_box_frame

    def add_mapping(self, mapping=None):
        if mapping is None:
            mapping = Mapping()
        print(f"Adding mapping: {mapping}")
        settings.add_mapping(mapping)
        self.draw_mapping_boxes()

    def remove_mapping(self, mapping):
        print(f"Removing mapping: {mapping}")
        settings.remove_mapping(mapping)
        self.draw_mapping_boxes()
