"""
Set the folder index the image should sort to and save it to sort_file.yaml.
"""
import argparse
import os.path
import pathlib
import shutil

import yaml

from main.Settings import settings


class SortFile:

    def __init__(self):
        self._filename = "sort_file.yaml"
        self._config = {}
        self.sort_file_path = None

    def init(self, folder):
        # initialize the sorted file, do this when sorting is started
        self.sort_file_path = os.path.expanduser(os.path.join(folder, self._filename))
        print(f"Sort file path set to {self.sort_file_path}")
        if os.path.isfile(self.sort_file_path):
            print("Reading existing sort file.")
            self.read_sort_file()

    def set_index(self, filename, index):
        """
        Set the sort of an image.
        :param filename: The image filename, without folder path.
        :param index: The index of the folder to move the image to.
        """
        if not isinstance(index, int):
            print(f"Tried to set sort to non-int type {index} ({type(index)}).")
            return
        self._config[filename] = index
        self.write_sort_file()

    def get_index(self, filename):
        if filename not in self._config.keys():
            return None
        index = self._config[filename]
        return index

    def read_sort_file(self):
        with open(self.sort_file_path) as file:
            self._config = yaml.safe_load(file)

    def write_sort_file(self):
        # write the sort to file
        with open(self.sort_file_path, "w") as file:  # overwrite
            yaml.safe_dump(self._config, file)

    def apply_sorting(self, source_folder, undo=False):
        # reads the sort file in the provided folder and moves its images accordingly using the settings file
        # undo=True reverses this action
        print(f"Applying sort file.")
        mappings = settings.get_mappings()
        for filename, index in self._config.items():
            mapping = mappings[index]
            source_image_path = os.path.expanduser(os.path.join(source_folder, filename))
            result_image_path = os.path.expanduser(os.path.join(mapping.folder, filename))

            exists = os.path.isfile(result_image_path)
            result_folder_name = mapping.get_folder_name()

            if undo:
                if not exists:
                    print(f"File {filename} not found in {result_folder_name}.")
                    continue
                print(f"Recovering {filename} from {result_folder_name}.")
                shutil.move(result_image_path, source_image_path)
            else:
                if exists:
                    print(f"File {filename} already exists in {result_folder_name}, file not moved.")
                    continue
                print(f"Moving {filename} to {result_folder_name}.")
                shutil.move(source_image_path, result_image_path)


sort_file = SortFile()

if __name__ == "__main__":
    # my own import break with cli usage
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--path", type=pathlib.Path, help="The folder where sort_file.yaml is located.")
    parser.add_argument("-a", "--apply", help="apply the sort",
                        action="store_true")
    parser.add_argument("-u", "--undo", help="undo the sort",
                        action="store_true", default=True)
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    args = parser.parse_args()

    path = args.path
    if path is None:
        print(f"No path given, using the one in the settings file.")
        path = settings.source_folder
    print(f"Using path: {path}")
    sort_file.init(path)
    sort_file.apply_sorting(path, undo=True)
