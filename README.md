# Photosorter
An image is shown. Press a key to move the image to a folder.  
The folders and the keys are customizable.  
Example configurations:
* press 'F' to move the image to the 'faces' folder
* press 'L' to move the image to the 'landscapes' folder

Photosorter saves the folder where the image should move to in a file: `sort_file.yaml`.  
**This allows you to close the application at any time, and undo/redo the sorting.**  
At any time use the shortcuts or the buttons in the menu bar to apply or undo the sort file.

## Demonstration
[YouTube](https://youtu.be/Mgmon0Z1ark)

## Features
* Loads next images into memory for a snappy feeling.
* Saves configuration to file.
* Saves sorted images paths to file.

## TODO
* check if pyqtwebengine and pywebview dependencies are really needed
* add count for amount of photos in folder marked
* start sorter from last marked photo
* add option to either copy or cut files to result dirs 

