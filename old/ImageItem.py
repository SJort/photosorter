"""
Class for representing one image to be loaded and shown.
Used to keep track of the full path, just the filename and the in memory loaded instance.

Losing the reference of a loaded image whilst it is being rendered on the canvas, deletes it from the canvas.
"""
import os
import threading

from PIL import ImageTk, Image


class ImageItem:
    def __init__(self, path):
        self.path = path
        self.filename = os.path.basename(path)
        # this should not be accessed from outside
        self._loaded_image = None
        self._loading_thread = None

    def __str__(self):
        return f"ImageItem({self.filename})"

    def load(self, max_width, max_height):
        # load the image into memory on current thread
        if self.is_loaded():
            return

        print(f"Loading {self.filename} to memory.")
        if not os.path.isfile(self.path):
            print(f"Tried to load non existing image: {self.path}")
            # self._loaded_image = "HACK"
            return
        with Image.open(self.path) as img:
            # resize image
            width_percent = (max_width / float(img.size[0]))
            height_size = int((float(img.size[1]) * float(width_percent)))
            if height_size > max_height:
                height_percent = (max_height / float(img.size[1]))
                width_size = int((float(img.size[0]) * float(height_percent)))
                img = img.resize((width_size, max_height), Image.ANTIALIAS)
            else:
                img = img.resize((max_width, height_size), Image.ANTIALIAS)

            # image could have been loaded from another source
            # if thats is the case, overwriting it would remove the existing image on the canvas
            if self._loaded_image is None:
                self._loaded_image = ImageTk.PhotoImage(image=img)
            return self._loaded_image

    def load_async(self):
        # load the image into memory on a new thread
        self._loading_thread = threading.Thread(target=self.load, args=())
        self._loading_thread.setDaemon(True)
        self._loading_thread.start()

    def get_image(self, width, height):
        # get the image, preloaded or not
        if not self.is_loaded():
            print(f"WARNING: {self.filename} was not loaded in memory yet, loading now.")
            self.load(width, height)
        return self._loaded_image

    def unload(self):
        if self.is_loaded():
            print(f"Unloading {self.filename} from memory.")
            self._loaded_image = None
        self._loading_thread = None

    def is_loaded(self):
        return self._loaded_image is not None
