"""
Wrapper for a thread which enables stopping and starting.
Usage and tests: see below.
TODO: allow thread functions to stop themselves by returning something
"""
import threading
import time


class StoppableThread:
    def __init__(self, function, on_start=None, on_stop=None, name="StoppableThread"):
        """
        Create a new Stoppable Thread object.
        :param function: The function to repeatedly call. The while True part of the function is already handled here.
        :param on_start: The function to call when the thread is started.
        :param on_stop: The function to call when the threads is stopped.
        :param name: The name of the thread, mainly for logging.
        """
        self.on_start = on_start
        self.on_stop = on_stop
        self.function = function
        self.name = name

        self._is_running = False  # set to stop to stop the thread, automatically set to true after stopped
        self._thread = None  # thread object initialized when started

    def function_wrapper(self):
        # the actual function called by the thread
        if self.on_start is not None:
            # called once when started
            self.on_start()

        while self._is_running:
            # continuously called
            self.function()

        if self.on_stop is not None:
            # called once when stopped
            self.on_stop()

        self._is_running = True

    def start(self):
        # Start the thread as daemon, so it is automatically killed when program is done or exited.
        if self._thread is not None:
            # Don't start a thread which is already running, otherwise memory leak I guess
            print(f"Thread already running: {self.name}")
        self._thread = threading.Thread(target=self.function_wrapper)
        self._thread.setDaemon(True)
        self._is_running = True
        self._thread.start()
        print(f"Thread started: {self.name}")

    def stop(self):
        if self._thread is None:
            print(f"Thread was not running: {self.name}")
            return

        # Stop the thread.
        self._is_running = False
        print(f"Waiting for thread to stop: {self.name}")
        while self._is_running is False:  # self.may_run is set to True after on_stop is called
            time.sleep(0.01)
        print(f"Thread stopped: {self.name}")
        self._thread = None  # delete the thread object


if __name__ == "__main__":
    def thread_fun_1():
        print("Thread 1")
        time.sleep(0.5)


    def thread_fun_2():
        print("Thread 2")
        time.sleep(0.5)

    def long_fun():
        for x in range(4):
            print(f"Long thread {x}")
            time.sleep(1)

    def fun_thread():
        time.sleep(2)
        thread = StoppableThread(long_fun, name="longthread")
        thread.start()


    thread1 = StoppableThread(thread_fun_1, name="Thread name 1")
    thread2 = StoppableThread(thread_fun_2, name="Don't thread on me #2")
    thread3 = StoppableThread(long_fun, name="3333")
    thread4 = StoppableThread(fun_thread, name="3333")
    thread3.start()
    time.sleep(1)
    thread4.start()
    time.sleep(5)

