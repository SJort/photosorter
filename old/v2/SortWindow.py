import threading
import time
import tkinter as tk

from main.FileManipulation import file_manipulation
from old.ImageLoader import image_loader
from main.Logger import logger
from main.Settings import settings
from main.Timer import *


class SortWindow:
    def __init__(self, frame):
        self.frame = frame

        self.frame.bind("<Key>", self.on_key)
        self.main_thread = None

        # top information frame
        self.info_frame = tk.Frame(self.frame)
        self.frame.grid_rowconfigure(1, weight=1)
        self.frame.grid_columnconfigure(0, weight=1)
        self.info_frame.grid(row=0, column=0, padx=10, pady=10, sticky=tk.NSEW)

        # image index label (3/8)
        self.count_label_text = tk.StringVar()
        self.count_label = tk.Label(self.info_frame, textvariable=self.count_label_text)

        self.info_frame.grid_rowconfigure(0, weight=1)
        self.info_frame.grid_columnconfigure(2, weight=1)
        self.count_label.grid(row=0, column=2, sticky=tk.E, padx=10)

        # image name label (girl.jpg)
        self.filename_label_text = tk.StringVar()
        self.filename_label = tk.Label(self.info_frame, textvariable=self.filename_label_text)

        self.info_frame.grid_rowconfigure(0, weight=1)
        self.info_frame.grid_columnconfigure(1, weight=1)
        self.filename_label.grid(row=0, column=1)

        # advertisement
        self.text_label = tk.Label(self.info_frame, text="Jorts PhotoSorter")

        self.info_frame.grid_rowconfigure(0, weight=1)
        self.info_frame.grid_columnconfigure(0, weight=1)
        self.text_label.grid(row=0, column=0, sticky=tk.W, padx=10)

        # canvas below information frame taking all available space
        self.canvas = tk.Canvas(self.frame)
        self.canvas.bind("<Configure>", self.resize_canvas)
        self.frame.grid_rowconfigure(1, weight=1)
        self.frame.grid_columnconfigure(0, weight=1)
        self.canvas.grid(row=1, column=0, columnspan=3, padx=10, pady=10, sticky=tk.NSEW)
        self.c_w = self.canvas.winfo_width()
        self.c_h = self.canvas.winfo_height()

        self.start_thread()

    def resize_canvas(self, event):
        print(f"Resizing canvas to {event.width}x{event.height}")
        image_loader.unload_all()
        image_loader.load_current_image(self.canvas)

    def load_preloaded_image(self, image):
        self.canvas.create_image(20, 20, anchor=tk.NW, image=image)

    def on_key(self, event):
        pressed_key = str(event.char)
        print(f"{get_elapsed('runtime')}: pressed {pressed_key}")

        # find the mapping of which shortcut matches the pressed key
        matching_mapping = None
        for mapping in settings.get_mappings():
            if pressed_key == mapping.shortcut:
                matching_mapping = mapping
                break

        # ignore and show alert if nothing matched
        if matching_mapping is None:
            logger.show_info(f"Pressed key '{pressed_key}' does not match the shortcut of any mapping.")
            return

        # move the image
        try:
            file_manipulation.move(image_loader.current_image.filename, matching_mapping)
        except Exception as e:
            print(f"Could not move image, probably not loaded yet: {e}")

        # load the next image
        image_loader.increase_index()
        image_loader.load_current_image(self.canvas)

    def label_updater(self):
        while True:
            text = f"Image {image_loader.calculated_index}/{image_loader.amount_of_images}"
            self.count_label_text.set(text)
            text = "Loading..."
            if image_loader.current_image is not None:
                text = f"{image_loader.current_image.filename}"
            self.filename_label_text.set(text)
            time.sleep(0.05)

    def start_thread(self):
        self.main_thread = threading.Thread(target=self.label_updater, args=())
        self.main_thread.setDaemon(True)
        self.main_thread.start()
