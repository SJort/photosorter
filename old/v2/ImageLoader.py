"""
Class responsible for rendering images on a canvas and reading the image folder.
Loads the next X images into memory on a separate thread, making rendering faster.
"""
import os
import threading
import time
import tkinter as tk
from pathlib import Path
from tkinter import CENTER

from main.ImageItem import ImageItem
from main.ThreadRestart import StoppableThread


class ImageLoader:
    def __init__(self):
        self.available_width = 300
        self.available_height = 300

        self.path = None
        self.index = 0
        self.calculated_index = 0
        self.max_next_loaded = 3
        self.max_prev_loaded = 2

        # main thread which dynamically loads the next few images into memory
        self.main_thread = StoppableThread(function=self._bg_img_loader_thread_function)
        # thread to load a single image into memory, for when it is not dynamically loaded already
        self.image_thread = None

        self.default_image = None
        self.empty_image = None

        self.current_image = None

        # just init to avoid problem :)
        self.image_paths = None
        self.image_paths = None
        self.image_items = None

        self.amount_of_images = 0

        print("Image loader initialized.")

    def load_default_images(self):
        # the image must be loaded after some sort of tkinter window is initialized
        self.default_image = ImageItem(path="loading.png")
        self.default_image.init(max_width=300, max_height=300)
        self.empty_image = ImageItem(path="empty.png")
        self.empty_image.init(max_width=300, max_height=300)

    def set_path(self, path):
        self.main_thread.stop()  # stop previous thread if it has started
        # BIG TODO
        self.path = os.path.expanduser(path)
        if not os.path.isdir(self.path):
            print(f"ERROR: {self.path} does not exist.")
            return

        print(f"Set image loader path to {self.path}")
        self.unload_all()
        # self.current_image = None

        # sort by creation date
        self.image_paths = sorted(Path(self.path).iterdir(), key=os.path.getmtime)
        # filter out non-images
        self.image_paths = [file for file in self.image_paths if self.is_image(str(file))]
        # convert to my image objects
        self.image_items = [ImageItem(path=image_path) for image_path in self.image_paths]

        self.amount_of_images = len(self.image_items)
        self.load_default_images()
        self.main_thread.start()

    def increase_index(self):
        self.index += 1
        self.calculated_index = self.index % self.amount_of_images

    def decrease_index(self):
        self.index -= 1
        self.calculated_index = self.index % self.amount_of_images

    def draw_on_canvas(self, canvas, image_item):
        if image_item is None:
            print("ERROR: Tried to draw None image on canvas.")
            return
        width = canvas.winfo_width()
        height = canvas.winfo_height()
        canvas.create_image(width // 2, height // 2, anchor=CENTER, image=image_item.get_image(width, height))
        self.current_image = image_item

    def _load_image_thread_function(self, canvas, image):
        self.draw_on_canvas(canvas, self.default_image)
        image.init(canvas.winfo_width(), canvas.winfo_height())
        self.draw_on_canvas(canvas, image)

    def load_current_image(self, canvas):
        if self.image_items is None:
            print(f"Image loader not initialized yet.")
            return
        if len(self.image_items) == 0:
            print(f"No images left in folder.")
            self.draw_on_canvas(canvas, self.empty_image)
            return
        image = self.image_items[self.calculated_index]
        print(f"Showing {self.calculated_index} (calculated from {self.index}): {image.filename}")
        canvas.delete("all")  # clear canvas, images otherwise stack
        if image.is_loaded():
            self.draw_on_canvas(canvas, image)
        else:
            self.image_thread = threading.Thread(target=self._load_image_thread_function, args=(canvas, image))
            self.image_thread.setDaemon(True)
            self.image_thread.start()

    @staticmethod
    def is_image(filename):
        return filename.lower().endswith(('.png', '.jpg', '.jpeg'))

    def unload_all(self):
        if self.image_items is None:
            return
        for image in self.image_items:
            image.unload()

    def _bg_img_loader_thread_function(self):
        # loads images into memory
        try:
            start = self.calculated_index - self.max_prev_loaded
            finish = self.calculated_index + self.max_next_loaded
            inverse = False
            if start < 0:
                start = self.amount_of_images + start
                start, finish = finish, start  # swap vars
                inverse = True

            # print(f"Range for {self.calculated_index}: {start} to {finish} (inverse={inverse})")
            for x in range(self.amount_of_images):
                load = False
                in_range = start < x < finish
                if inverse and not in_range:
                    load = True
                if not inverse and in_range:
                    load = True

                image = self.image_items[x]
                if load:
                    if image.is_loaded():
                        continue
                    print(f"Dynamically ", end="")
                    if not os.path.isfile(image.sort_file_path):
                        print(f"Trying to load an non existing image, stopping thread")
                        self.main_thread.stop()
                        break


                    image.init(self.available_width, self.available_height)
                    break  # break because the index may have changes whilst we are still iterating
                else:
                    image.unload()

            time.sleep(0.05)

        except FileNotFoundError as e:
            print(f"Image thread could not find the image: {e}")
            self.main_thread.stop()
        except Exception as e:
            print(f"Image loader thread exception: {e}.")


image_loader = ImageLoader()

if __name__ == "__main__":
    def on_key(event):
        pressed_key = str(event.char)
        print(f"Pressed {pressed_key}")
        if pressed_key == "1":
            print("Load path 1")
            path = "/home/jort/this/images/sorting/landscape"
            image_loader.set_path(path)
            image_loader.load_current_image(canvas)
        if pressed_key == "2":
            print("Load path 2")
            path = "/home/jort/this/images/sorting/serious"
            image_loader.set_path(path)
            image_loader.load_current_image(canvas)
        if pressed_key == "a":
            image_loader.increase_index()
            image_loader.load_current_image(canvas)


    root = tk.Tk()
    root.bind("<Escape>", lambda x: root.destroy())
    root.minsize(500, 400)
    root.bind("<Key>", on_key)
    canvas = tk.Canvas(root)
    canvas.pack()
    root.mainloop()
