"""
Log class which for now just can show messages in GUI.
Basically acts as an Control instance holder.
"""


class Logger:
    def __init__(self):
        self.show_info = None


logger = Logger()
