"""
Main class, PhotoSorter starts here.

Shows the configuration window.
Shows the sorting window

Hold the GUI variables and ties everything together.
When a key is pressed, communicates with ImageLoader to show the next image on the canvas.
"""
import os
import tkinter as tk

from old.ImageLoader import image_loader
from main.Logger import logger
from main.Settings import settings
from main.SetupWindow import SetupWindow
from main.SortWindow import SortWindow
from main.Timer import *


class Control:

    def __init__(self):
        start_timer("runtime")

        # root window
        self.root = tk.Tk()
        self.root.bind("<Escape>", lambda x: self.stop())
        self.root.minsize(500, 400)
        self.root.title("Jorts PhotoSorter")

        # information frame
        self.info_frame = tk.Frame(self.root)
        self.info_frame.grid(row=1, column=0, sticky=tk.NSEW)

        # information label
        self.info_label_text = tk.StringVar()
        self.info_label = tk.Label(self.info_frame, textvariable=self.info_label_text, fg="red")
        self.info_label.grid(row=0, column=0, sticky=tk.W)

        self.after_id = None  # id given when after() is called, keep it so we can cancel it
        logger.show_info = self.show_error

        self.setup_menu()

        # main frame
        self.current_frame = None
        self.root.grid_rowconfigure(0, weight=1)
        self.root.grid_columnconfigure(0, weight=1)

        # sort window
        self.sort_window_frame = tk.Frame(self.root)
        self.sort_window_frame.grid(row=0, column=0, sticky="news")
        self.sort_window = SortWindow(self.sort_window_frame)

        # setup window
        self.setup_window_frame = tk.Frame(self.root)
        self.setup_window_frame.grid(row=0, column=0, sticky="news")
        self.setup_window = SetupWindow(self.setup_window_frame, self)

        self.load_setup_window()

        self.setup_shortcuts()

        self.root.mainloop()

    def load_sorting_window(self, validate=True):
        # could do: only reload when source folder is changed
        print("Validating input...")
        if validate:
            if not os.path.isdir(settings.source_folder):
                logger.show_info(f"Source folder does not exist", color="red")

            for mapping in settings.get_mappings():
                folder = mapping.folder
                if not os.path.isdir(folder):
                    print(f"Not an existing folder: {folder}")
                    if settings.auto_create_folders:
                        os.mkdir(folder)
                        print(f"Created {folder}")
                    else:
                        logger.show_info(f"Folder does not exist {folder}", color="red")
                        return
        print("Loading sorting window")
        image_loader.set_path(settings.source_folder)
        self.load_frame(self.sort_window.frame)
        image_loader.load_current_image(self.sort_window.canvas)

    def stop(self):
        self.root.destroy()

    def load_frame(self, frame):
        self.current_frame = frame
        self.current_frame.tkraise()
        self.current_frame.focus_set()

    def load_setup_window(self):
        print("Loading setup window")
        self.load_frame(self.setup_window_frame)

    def setup_menu(self):
        menu = tk.Menu(self.root, relief="solid")
        self.root.config(menu=menu)

        file_menu = tk.Menu(menu, tearoff=False)
        menu.add_cascade(label="File", menu=file_menu)
        file_menu.add_command(label="Save", command=settings.save(), accelerator="Ctrl+S")
        file_menu.add_command(label="Setup", command=self.load_setup_window, accelerator="Ctrl+P")
        file_menu.add_separator()
        file_menu.add_command(label="Quit", command=self.stop, accelerator="Ctrl+Q")

        menu.add_command(label="Setup", command=self.load_setup_window)
        menu.add_command(label="Save", command=settings.save)
        check = tk.StringVar()
        menu.add_checkbutton(label="Auto", variable=check, onvalue=1, offvalue=0)
        menu.grid_rowconfigure(0, weight=1)
        menu.grid_columnconfigure(0, weight=1)

    def show_error(self, info, duration=2000, color="black"):
        print(f"Showing '{info}' in GUI in color {color} for {duration}ms.")
        # use the saved id to cancel the text remove effect
        if self.after_id is not None:
            self.root.after_cancel(self.after_id)
        self.info_label.configure(fg=color)
        self.info_label_text.set(info)
        self.after_id = self.root.after(duration, lambda: self.info_label_text.set(""))

    def setup_shortcuts(self):
        self.root.bind('<Control-s>', lambda e: settings.save())
        self.root.bind("<Control-q>", lambda e: self.stop())
        self.root.bind("<Control-p>", lambda e: self.load_setup_window())


if __name__ == "__main__":
    control = Control()
