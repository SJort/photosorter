"""
Class to launch a system native folder pick menu.
Return the folder path.
Example: see bottom
I can't be arsed to make this code cleaner
"""
import os.path

import webview


class FolderPicker:
    @staticmethod
    def pick(initial=None, stringvar_to_update=None, mapping_to_update=None):
        # launch native folder picker, update mapping and text field with result
        if initial is None or not os.path.isdir(initial):
            print(f"Initial path is not a directory: {initial}, using default.")
            initial = os.path.expanduser("~/Pictures")
        picker = FolderPickerInnerWrapThing(initial=initial)
        folder_path = picker.launch()
        print(f"Chosen folder: {folder_path}")
        if folder_path is None:
            print(f"Didn't pick folder.")
            return

        if mapping_to_update is not None:
            mapping_to_update.folder = folder_path
        if stringvar_to_update is not None:
            stringvar_to_update.set(folder_path)
        return folder_path


class FolderPickerInnerWrapThing:
    def __init__(self, initial=None):
        self.file = None
        self.initial_folder = initial

    def webview_file_dialog(self):

        def open_file_dialog(w):
            try:
                files = w.create_file_dialog(webview.FOLDER_DIALOG,
                                             allow_multiple=False,
                                             # dialog_type=10,
                                             directory=self.initial_folder if self.initial_folder is not None else "",
                                             )
                self.file = files[0]
            except TypeError:
                pass  # user exited file dialog without picking
            finally:
                w.destroy()

        window = webview.create_window(title="Choose a folder")
        webview.start(open_file_dialog, window)
        # file will either be a string or None
        return self.file

    def launch(self):
        return self.webview_file_dialog()


if __name__ == "__main__":
    p = FolderPicker(initial=None)
    result = p.launch()
    print(f"Result: {result}")
