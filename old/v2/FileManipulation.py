"""
Class to handle stuff about the files:
- move to destination
TODO:
- copy to destination
- history / undo
"""
import os
import shutil

from main.Logger import logger
from main.Settings import settings


class FileManipulation:
    def __init__(self):
        pass

    def move(self, filename, mapping):
        source_path = os.path.join(os.path.expanduser(settings.source_folder), filename)
        destination_path = os.path.join(os.path.expanduser(mapping.folder), filename)
        destination_name = destination_path.split("/")[-2]
        logger.show_info(f"Moved to {destination_name}")
        shutil.move(source_path, destination_path)
        print(f"Moved from {source_path} to {destination_path}")


file_manipulation = FileManipulation()
