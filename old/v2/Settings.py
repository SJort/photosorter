"""
Class for loading and saving settings.
Import this module and use the 'settings' variable to utilize.
Example usage: see bottom.
"""
import os.path
import traceback

from main.Logger import logger
from main.Mapping import Mapping
import yaml


class Settings:

    def __init__(self):
        self._source_folder = "~/Pictures"
        self._mappings = []
        self._filename = "config.yaml"
        self._config = {}
        self._auto_create_folders = False
        self.load()  # load the settings from file on class initialization

    def write_dict(self):
        # write the instanced variables to the dictionary, so the dict can be saved as yaml
        self._config = {"source_folder": self._source_folder,
                        "auto_create_folders": self._auto_create_folders}

        index = 0
        for mapping in self._mappings:
            self._config[f"mapping{index}"] = {"shortcut": mapping.shortcut, "folder": mapping.folder}
            index += 1

    def read_dict(self):
        # convert the values in the dictionary to class variables for easy access
        self._source_folder = self.read_key("source_folder")
        self.auto_create_folders = self.read_key("auto_create_folders")
        print(f"Loaded {self._auto_create_folders} {type(self._auto_create_folders)}")
        print(f"Parsed source folder: {self._source_folder}")
        for key in self._config.keys():
            if key.startswith("mapping"):
                dict = self.read_key(key)
                read_mapping = Mapping(shortcut=dict["shortcut"], folder=dict["folder"])
                self._mappings.append(read_mapping)
                print(f"Parsed mapping '{key}': {read_mapping}")
        self.write_dict()  # get the mapping index straight (in case user edited the yaml file)

    def read_key(self, key):
        # save method to read a key
        if key not in self._config.keys():
            print(f"Key not found in config: {key}")
            return
        return self._config[key]

    def save(self):
        # write the configurations to the yaml file
        self.write_dict()
        with open(self._filename, "w") as file:  # overwrite
            yaml.safe_dump(self._config, file)
        if logger.show_info is not None:  # it is none when application is loaded
            logger.show_info(f"Settings saved as {self._filename}")
        print(f"Saved settings.")

    def create_sample_config(self):
        # create a sample basic configuration, if the user deleted it
        print(f"Creating sample configuration file")
        try:
            self.source_folder = self._source_folder
            self.auto_create_folders = False
            self.add_mapping(Mapping("~/Pictures/sample-result-people"))
            self.add_mapping(Mapping("~/Pictures/sample-result-landscapes"))
            self.add_mapping(Mapping("~/Pictures/sample-result-duplicates"))
            self.save()
            print(f"Created sample configuration file.")
        except Exception as e:
            print(f"Could not create sample configuration file: {e}")
            print(traceback.format_exc())

    def load(self):
        # load the saved yaml file

        if not os.path.isfile(self._filename):
            # create sample yaml file if none exist
            self.create_sample_config()

        try:
            with open(self._filename) as file:
                self._config = yaml.safe_load(file)
                self.read_dict()
        except Exception as e:
            print(f"Could not load settings! Exception: {e}")
            print(traceback.format_exc())

        print("Loaded settings.")

    # following functions are getters and setters, for potential future debugging or save-on-modify purposes
    def add_mapping(self, mapping):
        self._mappings.append(mapping)

    def remove_mapping(self, mapping):
        self._mappings.remove(mapping)

    def get_mappings(self):
        return self._mappings

    @property
    def source_folder(self):
        return self._source_folder

    @source_folder.setter
    def source_folder(self, value):
        self._source_folder = value

    @property
    def auto_create_folders(self):
        return self._auto_create_folders

    @auto_create_folders.setter
    def auto_create_folders(self, value):
        self._auto_create_folders = value


# create an instance of settings, which acts as a singleton when this module is imported
settings = Settings()

if __name__ == "__main__":
    mapping1 = Mapping("/home/jort/this/images/sorting", "a")
    mapping2 = Mapping("/home/jort/this/images/sorting/happy", "s")
    mapping3 = Mapping("/home/jort/this/images/sorting/serious cake", "d")
    mappings = [mapping1, mapping2, mapping3]
    settings.load()
    settings.save()
