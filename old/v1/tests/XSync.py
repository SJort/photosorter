import os
from datetime import datetime
from tkinter import Label, Image

from PIL import ImageTk


class One():
    def __init__(self):
        self.directory = "/home/this/this/PhotoSortingTest"
        self.files = []
        self.go()
        self.file = None

    def go(self):
        for file in os.listdir(self.directory):
            self.file = os.fsdecode(file)
            if self.file.endswith(".JPG") or self.file.endswith(".PNG") or self.file.endswith(".jpg"):
                path = self.directory + "/" + self.file
                print("Found media file:", path)
                self.files.append(open(path, buffering=-1))


        def show_image(self, path):
            self.panel.destroy()
            base_width = 500
            self.img = Image.open(path)
            width_percent = (base_width / float(self.img.size[0]))
            height_size = int((float(self.img.size[1]) * float(width_percent)))
            self.img = self.img.resize((base_width, height_size), Image.ANTIALIAS)
            # self because otherwise the reference gets lost and image wont show
            self.img = ImageTk.PhotoImage(self.img)
            self.panel = Label(self, image=self.img)
            self.panel.grid(row=0, column=0)

start = datetime.now()
print(start)
One()
end = datetime.now()
print(end - start)