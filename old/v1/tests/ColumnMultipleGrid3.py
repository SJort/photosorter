from tkinter import *

root = Tk()

loginLabel = Label(root, text="Login")
passwordLabel = Label(root, text="Password")

loginEntry = Entry(root)
passwordEntry = Entry(root)

# it assumes the first column if not specified
loginLabel.grid(row=0, sticky=E) #north East south west, here it means; right align this
passwordLabel.grid(row=1, sticky=E)

loginEntry.grid(row=0, column=1)
passwordEntry.grid(row=1, column=1)

checkbox = Checkbutton(root, text="Keep signed in")
# let an item take up multiple columns
checkbox.grid(columnspan=2)

root.mainloop()