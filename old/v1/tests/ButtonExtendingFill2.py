from tkinter import *

root = Tk()

label1 = Label(root, text="Labelle 1", bg="#449933", fg="white")
label1.pack()

label2 = Label(root, text="Labelle 2", bg="red", fg="white")
# fill as much the x value of the parent is (the window here)
label2.pack(fill=X);

label3 = Label(root, text="Labelle 3", bg="blue", fg="white")
label3.pack(fill=Y, side=LEFT)


root.mainloop()