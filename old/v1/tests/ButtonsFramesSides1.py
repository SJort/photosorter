from tkinter import *

# create a blank window
root = Tk()

#make invisible container in root
topFrame = Frame(root)

# put the frame in the first available space in the window
topFrame.pack()

bottomFrame = Frame(root)

bottomFrame.pack(side=BOTTOM)

# fg = foreground
button1 = Button(topFrame, text="TopButton", fg="red")
button2 = Button(bottomFrame, text="BottomButton", fg ="green")
button3 = Button(bottomFrame, text="BottomButton2", fg ="#985294")

# take button1, and put is as far left as possible
button1.pack(side=LEFT)

button2.pack(side=RIGHT)
button3.pack(side=RIGHT)


# put the window in an infinite loop, so it doesn't close immediately
root.mainloop()



