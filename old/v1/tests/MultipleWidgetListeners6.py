from tkinter import *

root = Tk()


def leftClick(event):
    print("leftclick")


def middleClick(event):
    print("mids")


def rightClick(event):
    print("rightclicc")


def on_key(event):
    print("pressed", repr(event.char))


# this basically resizes the window
frame = Frame(root, width=300, height=200)
frame.focus_set()

# bind shit to that widget
frame.bind("<Button-1>", leftClick)
frame.bind("<Button-2>", middleClick)
frame.bind("<Button-3>", rightClick)
frame.bind("<Key>", on_key)

frame.pack()

root.mainloop()
