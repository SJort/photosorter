# Multi-frame tkinter application v2.3
import tkinter as tk

class SampleApp(Tk):
    def __init__(self):
        __init__(self)
        self._frame = None
        self.switch_frame(StartPage)

    def switch_frame(self, frame_class):
        """Destroys current frame and replaces it with a main one."""
        new_frame = frame_class(self)
        if self._frame is not None:
            self._frame.destroy()
        self._frame = new_frame
        self._frame.pack()

class StartPage(Frame):
    def __init__(self, controller):
        Frame.__init__(self, controller)
        Label(self, text="This is the start page").pack(side="top", fill="x", pady=10)
        Button(self, text="Open page one",
               command=lambda: controller.load_frame(PageOne)).pack()
        Button(self, text="Open page two",
               command=lambda: controller.load_frame(PageTwo)).pack()

class PageOne(Frame):
    def __init__(self, controller):
        Frame.__init__(self, controller)
        Label(self, text="This is page one").pack(side="top", fill="x", pady=10)
        Button(self, text="Return to start page",
               command=lambda: controller.load_frame(StartPage)).pack()

class PageTwo(Frame):
    def __init__(self, controller):
        Frame.__init__(self, controller)
        Label(self, text="This is page two").pack(side="top", fill="x", pady=10)
        Button(self, text="Return to start page",
               command=lambda: controller.load_frame(StartPage)).pack()

if __name__ == "__main__":
    app = SampleApp()
    app.mainloop()