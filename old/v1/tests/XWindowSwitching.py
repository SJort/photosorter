# Multi-frame tkinter application v2.3
from tkinter import *


# lambda arguments separated with commas: output

class Controller(Tk):
    def __init__(self):
        Tk.__init__(self)
        self.frame = None
        self.load_frame(StartPage)
        mainloop()

    def load_frame(self, frame_class):
        """Destroys current frame and replaces it with a main one."""
        new_frame = frame_class(self)
        if self.frame is not None:
            self.frame.destroy()
        self.frame = new_frame
        self.frame.pack()

    def placeholder(self):
        print("Placeholder definition")

    def load_start_frame(self):
        self.load_frame(StartPage)


class Window(Frame):
    def __init__(self, controller):
        self.controller = controller
        Frame.__init__(self, controller)


class StartPage(Window):
    def __init__(self, controller):
        super().__init__(controller)
        Label(self, text="This is the start page").pack(side="top", fill="x", pady=10)
        Button(self, text="Open page one",
               command=lambda: controller.load_frame(PageOne)).pack()
        Button(self, text="Open page two",
               command=lambda: controller.load_frame(PageTwo)).pack()


class PageOne(Window):
    def __init__(self, controller):
        super().__init__(controller)
        Label(self, text="This is page one").pack(side="top", fill="x", pady=10)
        button = Button(self, text="Return to start page", command=lambda: controller.load_frame(StartPage))
        button.pack()


class PageTwo(Window):
    def __init__(self, controller):
        super().__init__(controller)
        Label(self, text="This is page two").pack(side="top", fill="x", pady=10)
        Button(self, text="Return to start page", command=lambda: controller.load_frame(StartPage)).pack()


app = Controller()
