from tkinter import *

root = Tk()

def printName():
    print("hey, LOL")

# event = something that occurs by the user
def printNameWithEvent(event):
    print("crab?")

# define a method to call, DONT INCLUDE PARENTHESES
button1 = Button(root, text="cliccc", command=printName)
button1.pack()

button2 = Button(root, text="crab")
#bind( whatEvent?, whatFunction?)
#button -1 is the left mouse button
button2.bind("<Button-1>", printNameWithEvent)
button2.pack()

root.mainloop()