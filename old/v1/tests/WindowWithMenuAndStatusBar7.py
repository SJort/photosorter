from tkinter import *

root = Tk()

def doNothing():
    print("nothicc")


menu = Menu(root)
root.config(menu=menu)

#tearoff removes dashed line
fileMenu = Menu(menu, tearoff=0)
menu.add_cascade(label="File", menu=fileMenu)
fileMenu.add_command(label="Add", command=doNothing)
fileMenu.add_separator()
fileMenu.add_command(label="Exit", command=doNothing)

root.mainloop()