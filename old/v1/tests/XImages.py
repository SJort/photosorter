from tkinter import *
from PIL import ImageTk, Image

# This creates the main window of an application
window = Tk()
window.title("ImageDemo")

base_width = 300
# this removes those borders
# window.geometry(str(base_width) + "x" + str(base_width))
path = "../img/testBeast.jpg"

img = Image.open(path)
width_percent = (base_width / float(img.size[0]))
height_size = int((float(img.size[1]) * float(width_percent)))
img = img.resize((base_width, height_size), Image.ANTIALIAS)

img = ImageTk.PhotoImage(img)

panel = Label(window, image=img)

panel.pack()

window.mainloop()
