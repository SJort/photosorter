from tkinter import *


class Lmao:

    # special function called automatically to initialize
    def __init__(self, controller): # controller = root/main window
        frame = Frame(controller)
        frame.pack()

        self.printButton = Button(controller, text="pntbtn", command=self.printMessage)
        self.printButton.pack(side=LEFT)

        # call build in quit function, it breaks the mainloop
        self.quitButton = Button(controller, text="quitcc", command=controller.destroy)
        self.quitButton.pack(side=LEFT)

    # self means: whenever we create a Lmao object, self becomes that object
    def printMessage(self):
        print("pls work")



root = Tk()
klass = Lmao(root)

root.mainloop()