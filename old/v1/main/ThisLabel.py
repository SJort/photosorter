from tkinter import Label

from old.v1.main import ThisConstants


class ThisLabel(Label):

    def __init__(self, *args, **kwargs):
        Label.__init__(self, *args, **kwargs)
        self.configure(fg=ThisConstants.foreground_color, bg=ThisConstants.background_color)

