from tkinter import Entry

from old.v1.main import ThisConstants


class ThisEntry(Entry):

    def __init__(self,*args, **kwargs):
        super().__init__(*args, **kwargs)
        self.configure(bg=ThisConstants.background_color, fg=ThisConstants.foreground_color, highlightcolor=ThisConstants.foreground_color, highlightbackground=ThisConstants.foreground_color, highlightthickness=2, )
