from tkinter import *

from old.v1.main import ThisConstants
from old.v1.main.ThisButton import ThisButton


class ShortcutChooser(Tk):

    def __init__(self, rule, button):
        super().__init__()
        print("Press a shortcut key.")
        self.rule = rule
        self.button = button
        self.bind("<Key>", self.on_key)
        self.title("Command input")
        self.geometry("300x300")
        self.configure(bg=ThisConstants.background_color)
        label = Label(self, text="Press a key to use as shortcut", fg=ThisConstants.foreground_color, bg=ThisConstants.background_color)
        label.place(relx=.5, rely=.5, anchor="center")
        button = ThisButton(self, text="Cancel", command=self.destroy)
        button.place(relx=0.5, rely=1, anchor=S)
        self.focus_force()
        self.mainloop()

    def on_key(self, event):
        pressed_key = repr(event.char)
        self.rule.shortcut = pressed_key
        self.destroy()
        print("You choose", self.rule.shortcut, "as shortcut.")
        self.button.configure(text=self.rule.shortcut)



