import shutil
from tkinter import *
from old.v1.main.Window import Window

import os
from PIL import Image, ImageTk


class SortWindow(Window):

    def __init__(self, controller):
        super().__init__(controller)
        self.image_panel = Label(self)
        self.image = None
        self.current_image_filename = None
        # self.show_image(path="../img/testBeast.jpg")
        self.path = None
        self.directory = os.fsencode(self.controller._source_folder)
        self.show_next_image()
        self.attach_key_listener()
        self.focus_set()

    def show_next_image(self):
        print("Showing next image...")
        for file in os.listdir(self.directory):
            self.current_image_filename = os.fsdecode(file)
            if self.current_image_filename.endswith(".JPG") or self.current_image_filename.endswith(".PNG") or self.current_image_filename.endswith(".jpg"):
                self.path = self.directory.decode() + "/" + self.current_image_filename
                print(self.path)
                print("Found media file:", self.current_image_filename)
                break
        print("Done showing next image.")
        if self.path is not None:
            self.show_image(self.path)

    def attach_key_listener(self):
        self.bind("<Key>", self.on_key)

    def on_key(self, event):
        pressed_key = repr(event.char)
        print("Pressed ", pressed_key)

        if self.path is None:
            print("Can't find photos in given source directory!")
            return

        for i in range(len(self.controller.rules)):
            print("Comparing ", self.controller.rules[i].shortcut, " with pressed key ", pressed_key)
            # == compares for equality
            if self.controller.rules[i].shortcut == pressed_key:
                print("Moving", self.path, "to", self.controller.rules[i].folder)
                shutil.move(self.path, self.controller.rules[i].folder)
                self.show_next_image()

    def show_image(self, path):
        self.image_panel.destroy()
        base_width = 500
        self.image = Image.open(path)
        width_percent = (base_width / float(self.image.size[0]))
        height_size = int((float(self.image.size[1]) * float(width_percent)))
        self.image = self.image.resize((base_width, height_size), Image.ANTIALIAS)
        # self because otherwise the reference gets lost and image wont show
        self.image = ImageTk.PhotoImage(self.image)
        self.image_panel = Label(self, image=self.image)
        self.image_panel.grid(row=0, column=0)


    def on_resize_image(self, event):
        width_percent = (event.width / float(self.image.size[0]))
        height_size = int((float(self.image.size[1]) * float(width_percent)))
        self.image = self.img_copy.resize((event.width, height_size))

        self.background_image = ImageTk.PhotoImage(self.image)
        self.background.configure(image=self.background_image)
