import os

from old.v1.main.RuleBox import RuleBox
from old.v1.main.ThisButton import ThisButton
from old.v1.main.ThisLabel import ThisLabel
from old.v1.main.Window import Window


class SetupWindow(Window):
    used_rule_rows = 3
    main_column = 0
    new_rule_button = None
    start_button = None

    def __init__(self, controller):
        super().__init__(controller)

        # define buttons first because add_rule assumes they exist
        self.source_label = ThisLabel(self, text="Source folder")
        self.source_folder_box = None
        self.setup_source_folder_box()
        self.rule_label = ThisLabel(self, text="Rules")
        self.new_rule_button = ThisButton(self, text="Add main rule", command=self.add_rule)
        self.start_button = ThisButton(self, text="Start sorting", command=self.on_start)
        self.add_rule()

    def setup_source_folder_box(self):
        self.source_folder_box = RuleBox(self)
        self.source_folder_box.shortcut_button.destroy()
        self.source_folder_box.delete_button.destroy()
        # self.controller.source_folder = self.source_folder_box.rule.folder

    # shifts the buttons under the rules for when a main rule is added
    def shift_buttons(self):
        self.source_label.grid(row=0, column=self.main_column)
        self.source_folder_box.grid(row=1, column=self.main_column)
        self.rule_label.grid(row=2, column=self.main_column)
        self.new_rule_button.grid(row=self.used_rule_rows + 1, column=self.main_column)
        self.start_button.grid(row=self.used_rule_rows + 2, column=self.main_column)

    # adds a main rule to the interface
    def add_rule(self):
        # create frame that contains 1 rule
        rule_frame = RuleBox(self)
        rule_frame.grid(row=self.used_rule_rows, column=self.main_column)
        self.used_rule_rows += 1

        # shift bottom buttons so they don't overlap with the main rule
        self.shift_buttons()
        self.controller.rules.append(rule_frame.rule)
        # MAKE RULEBOX ARRAY AND COLLECT WHEN STARTING TO SORT

    def placeholder(self):
        print("PLACEHOLDER")

    def on_start(self):
        print("You pressed the start button!")

        self.controller._source_folder = self.source_folder_box.rule.folder
        if os.path.isdir(self.controller._source_folder):
            print("Using", self.controller._source_folder, "as source folder.")
        else:
            print("Source folder", self.controller._source_folder, "is invalid!")
            return

        print("You set the following rules:")
        for rule in self.controller.rules:
            print(rule.print())

        self.controller.load_photo_window()
