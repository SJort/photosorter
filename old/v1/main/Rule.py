
class Rule:

    def __init__(self, folder="folder", shortcut="shortcut"):
        self.folder = folder
        self.shortcut = shortcut

    def print(self):
        print("Shortcut", self.shortcut, "to folder", self.folder)

