from tkinter import *
from tkinter import filedialog

from old.v1.main.Rule import Rule
from old.v1.main.ShortcutChooser import ShortcutChooser
from old.v1.main.ThisButton import ThisButton
from old.v1.main.ThisFrame import ThisFrame


class RuleBox(ThisFrame):

    def __init__(self, root):
        ThisFrame.__init__(self, root)
        self.root = root
        self.rule = Rule(folder="NoFolderDefined", shortcut="NoShortcutDefined")

        # shortcut button
        self.shortcut_button = None
        self.setup_shortcut_button()

        # folder location entry
        self.folder_entry_sv = None
        self.entry = None
        self.setup_folder_entry()

        # pick folder button
        self.folder_button = None
        self.setup_pick_folder_button()

        # delete button
        self.delete_button = None
        self.setup_delete_button()

    def setup_shortcut_button(self):
        self.shortcut_button = ThisButton(self, text="Set",
                                          command=lambda: ShortcutChooser(self.rule, self.shortcut_button))
        self.shortcut_button.grid(row=0, column=0)
        self.shortcut_button.configure(width=3)

    def setup_folder_entry(self):
        self.folder_entry_sv = StringVar()

        def on_folder_entry_edit():
            print("Edited folder entry to", self.folder_entry_sv.get())
            self.rule.folder = self.folder_entry_sv.get()
            return True  # return true if change is accepted

        self.entry = Entry(self, textvariable=self.folder_entry_sv, validate="key",  # validate first was "focusout"
                           validatecommand=on_folder_entry_edit)
        self.entry.grid(row=0, column=1)

    def setup_pick_folder_button(self):
        self.folder_button = ThisButton(self, text="Choose", command=self.choose_folder)
        self.folder_button.grid(row=0, column=2)

    def setup_delete_button(self):
        self.delete_button = ThisButton(self, text="X", command=self.delete_rule)
        self.delete_button.grid(row=0, column=3)

    def choose_folder(self):
        print("Opening folder picker...")
        file_path = filedialog.askdirectory(initialdir=self.root.controller._source_folder)
        print(file_path)
        self.rule.folder = file_path
        print("File path in folder: ", self.rule.folder)
        self.folder_entry_sv.set(self.rule.folder)

    def delete_rule(self):
        self.root.controller.remove_rule(self.rule)
        self.destroy()
