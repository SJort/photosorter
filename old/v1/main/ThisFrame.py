from tkinter import Frame

from old.v1.main import ThisConstants


class ThisFrame(Frame):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.configure(bg=ThisConstants.background_color)
