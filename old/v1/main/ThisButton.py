from tkinter import Button

from old.v1.main import ThisConstants


class ThisButton(Button):

    def __init__(self, *args, **kwargs):
        Button.__init__(self, *args, **kwargs)
        self.configure(relief="solid", highlightcolor=ThisConstants.foreground_color, highlightbackground=ThisConstants.foreground_color, highlightthickness=2, fg=ThisConstants.foreground_color, bg=ThisConstants.background_color)

