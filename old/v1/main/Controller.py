from tkinter import *

from old.v1.main import ThisConstants
from old.v1.main.SetupWindow import SetupWindow
from old.v1.main.SortWindow import SortWindow

"""
een lijst met rules, elke keer als een add rule wordt gedaan wordt een aparte method geroepen om alle rules + buttons te shiften
ASyncIO -> queue
AIOConsole


"""


class Controller(Tk):
    rules = []
    source_folder = "/home/this/temp/source"

    def __init__(self):
        super().__init__()
        self.frame = None
        self.configure(bg=ThisConstants.background_color)
        self.title("THIS PhotoSorter")
        self.load_frame(SetupWindow)
        # self.load_frame(SortWindow)
        self.setup_menu()
        self.geometry("600x600")
        self.mainloop()

    def load_frame(self, frame_class):
        new_frame = frame_class(self)
        if self.frame is not None:
            self.frame.destroy()
        self.frame = new_frame
        # self.frame.grid(row=0, column=0)
        self.frame.place(relx=0.5, rely=0.5, anchor=CENTER)

    def setup_menu(self):
        menu = Menu(self, bg=ThisConstants.background_color, fg=ThisConstants.foreground_color)
        self.config(menu=menu)

        file_menu = Menu(menu, tearoff=0, bg=ThisConstants.background_color, fg=ThisConstants.foreground_color)
        menu.add_cascade(label="File", menu=file_menu)
        file_menu.add_command(label="Exit application", command=self.close_application)
        file_menu.add_command(label="Setup", command=self.load_setup_window())

    def load_setup_window(self):
        print("Loading SetupWindow...")
        self.load_frame(SetupWindow)

    def load_photo_window(self):
        print("Loading PhotoWindow...")
        self.load_frame(SortWindow)

    def close_application(self):
        print("Closing application...")
        self.destroy()

    def placeholder(self):
        print("Placeholder definition!")

    def remove_rule(self, rule):
        self.rules.remove(rule)
        print("You just removed the following rule:")
        rule.print()


Controller()
