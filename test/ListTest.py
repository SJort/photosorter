li = [1, 2, 3, 4, 5, 6, 7, 8, 9]

test1 = li[0:2]
test2 = li[2:4]

count = -3
total = 8

test3 = count % total

print(f"Test 1: {test1}")
print(f"Test 2: {test2}")
print(f"Test 3: {test3}")
