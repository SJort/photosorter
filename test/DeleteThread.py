import multiprocessing
import time


def func(start):
    count = start
    while True:
        print(f"Thread {count}")
        time.sleep(1)


processes = []
processes.append(multiprocessing.Process(target=func, args=(1,)))
processes.append(multiprocessing.Process(target=func, args=(2,)))
processes.append(multiprocessing.Process(target=func, args=(3,)))

for proces in processes:
    proces.start()

time.sleep(3)
for process in processes[0:1]:
    process.terminate()
