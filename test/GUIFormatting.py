import tkinter as tk

root = tk.Tk()
root.bind("<Escape>", lambda x: root.destroy())
root.geometry("400x400")

for x in range(3):
    root.grid_rowconfigure(x, weight=1)
    root.grid_columnconfigure(x, weight=1)

label = tk.Label(text="Jort photosorter", borderwidth=2, relief="solid")
label.grid(column=0, row=0, sticky=tk.W)

label = tk.Label(text="a long filename", borderwidth=2, relief="solid")
# root.grid_rowconfigure(0, weight=1)
# root.grid_columnconfigure(0, weight=1)
label.grid(column=1, row=0)

label = tk.Label(text="Image 3/4", borderwidth=2, relief="solid")
label.grid(column=2, row=0, sticky=tk.E)

root.mainloop()
