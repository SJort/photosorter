import threading
from tkinter import Tk

from PIL import Image, ImageTk


def load(paths):
    res = []
    for path in paths:
        print(f"Loading {path} to memory.")
        with Image.open(path) as img:
            loaded_image = ImageTk.PhotoImage(image=img)
            # print(f"Done loading {path}.")
            res.append(loaded_image)
    return res



def load_thread(paths):
    thread = threading.Thread(target=load, args=(paths,))
    thread.setDaemon(True)
    thread.start()
    return thread


root = Tk()
root.bind("<Escape>", lambda x: root.destroy())
root.geometry("400x400")
paths = (
    "img/img1.JPG",
    "img/img2.JPG",
    "img/img3.JPG",
    "img/img4.JPG",
    "img/img5.JPG",
)
load_thread(paths)
root.mainloop()
