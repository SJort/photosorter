import threading
import time


def runner(start):
    try:
        count = start
        for x in range(4):
        # while True:
            count += 1
            print(f"Count: {count}")
            time.sleep(1)
    except KeyboardInterrupt:
        print("Thread stopped")


thread = threading.Thread(target=runner, args=(100,))
thread.setDaemon(True)
thread.start()
while True:
    print(f"Thread alive: {thread.is_alive()}")
    time.sleep(1)

