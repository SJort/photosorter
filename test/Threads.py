"""
Create a thread and pass an single argument (comma required)
x = threading.Thread(target=func, args=(10,))

Await thread: x.join()


"""
import threading
import time


ls = []

def func(text):
    print("Start")
    # time.sleep(1)
    # print(text)
    for x in range(100000):
        x ^ x
    print("Finish")


def count(n, sleep):
    for i in range(1, n + 1):
        print(i)
        ls.append(i)
        time.sleep(sleep)


x = threading.Thread(target=count, args=(10, 0.2))
y = threading.Thread(target=count, args=(10, 0.5))
x.start()
y.start()
# x.join()
print("x joined")
# y.join()
print("y joined")


print("Done")
print(ls)
