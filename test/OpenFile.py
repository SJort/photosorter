import webview


def webview_file_dialog():
    file = None

    def open_file_dialog(w):
        nonlocal file
        try:
            files = w.create_file_dialog(webview.FOLDER_DIALOG,
                                         allow_multiple=False,
                                         # dialog_type=10,
                                         directory="/home/jort/this/",
                                         )
            file = files[0]
        except TypeError:
            pass  # user exited file dialog without picking
        finally:
            w.destroy()

    window = webview.create_window(title="Choose a folder", hidden=False)
    webview.start(open_file_dialog, window)
    # file will either be a string or None
    return file


print(webview_file_dialog())
