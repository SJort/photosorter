"""
async
- makes a function return a coroutine, which allows the function to run async
- so: anything with async prefix = coroutine

await
- executes a coroutine (returned by async functions)
- literally waits for the function to finish
- can only be used in async functions

event-loop
- lower level loop which handles async functions
- can be started from sequential code by doing asyncio.run(async function)

task
- launches an coroutine in the background, instead of awaiting for it to finish
- task = asyncio.create_task(async function)
- BUT only one async thing can happen at a time, so when create_task is run, it still needs to wait for
whatever function called create_task is done running, OR takes a break (the parent function uses await)

"""
import asyncio


async def main():
    print("Start")
    task = asyncio.create_task(foo("hoi"))
    for x in range(100000000):
        x * x
    print("Finish")


async def foo(text):
    print(text)
    await asyncio.sleep(1)


asyncio.run(main())
