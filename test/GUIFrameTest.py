import tkinter as tk


class Main:
    def __init__(self):
        self.root = tk.Tk()
        self.root.bind("<Escape>", lambda x: self.root.destroy())

        # self.frame = tk.Frame(self.root, bd=2, relief="solid")

        self.root.grid_rowconfigure(0, weight=1)
        self.root.grid_columnconfigure(0, weight=1)
        self.frame1 = tk.Frame(self.root, bd=2, relief="solid")

        self.frame1.grid_rowconfigure(0, weight=1)
        self.frame1.grid_columnconfigure(0, weight=1)
        label1 = tk.Label(self.frame1, text="Frame 1")
        label1.grid(row=0, column=0, sticky='news')
        button1 = tk.Button(self.frame1, text="Load frame 2", command=self.load_frame_2)
        button1.grid(row=1, column=0, sticky='news')

        self.frame2 = tk.Frame(self.root, bd=2, relief="solid", background="red")
        label2 = tk.Label(self.frame2, text="This is frame 2")
        label2.grid(row=0, column=0, sticky='news')
        button2 = tk.Button(self.frame2, text="Load first frame", command=self.load_frame_1)
        button2.grid(row=1, column=0, sticky='news')


        self.root.grid_rowconfigure(0, weight=1)
        self.root.grid_columnconfigure(0, weight=1)
        # self.frame1.pack(expand=1, fill=tk.BOTH)
        # self.frame2.pack(expand=1, fill=tk.BOTH)
        self.frame1.grid(row=0, column=0, sticky='news')
        self.frame2.grid(row=0, column=0, sticky='news')

        self.load_frame(self.frame1)
        self.root.mainloop()

    def load_frame_1(self):
        print("Loading frame 1")
        self.load_frame(self.frame1)

    def load_frame_2(self):
        print("Loading frame 2")
        self.load_frame(self.frame2)

    def load_frame(self, frame):
        self.frame = frame
        self.frame.tkraise()

        # self.frame.place(relx=0.5, rely=0.5, anchor=tk.CENTER)


if __name__ == "__main__":
    Main()
