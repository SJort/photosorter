class Mapping:
    def __init__(self, folder="folder", shortcut="shortcut"):
        self.folder = folder
        self.shortcut = shortcut

    def __str__(self):
        return f"Mapping: '{self.shortcut}' to {self.folder}"

    def serialize(self):
        return f"{self.shortcut} , {self.folder}"

    @staticmethod
    def deserialize(serial):
        split = [x.strip() for x in serial.split(",")]
        shortcut = split[0]
        folder = split[1]
        return Mapping(shortcut=shortcut, folder=folder)

    @staticmethod
    def serialize_to_file(mappings, file_path):
        print(f"Writing mappings to {file_path}")
        with open(file_path, "w") as file:
            for mapping in mappings:
                serial_line = mapping.serialize() + "\n"
                print(f"Writing {serial_line}", end="")
                file.write(serial_line)

    @staticmethod
    def deserialize_to_file(file_path):
        print(f"Reading mappings from {file_path}")
        mappings = []
        with open(file_path) as file:
            for line in file:
                mapping = Mapping.deserialize(line)
                print(f"Read from file: {mapping}")
                mappings.append(mapping)
        return mappings


class Test:
    def __init__(self):
        mapping1 = Mapping("/home/jort/this/images/sorting", "a")
        mapping2 = Mapping("/home/jort/this/images/sorting/happy", "s")
        mapping3 = Mapping("/home/jort/this/images/sorting/serious", "d")
        mappings = [mapping1, mapping2, mapping3]
        Mapping.serialize_to_file(mappings, "config.csv")
        mappings = Mapping.deserialize_to_file("config.csv")
        for mapping in mappings:
            print(mapping)


Test()
