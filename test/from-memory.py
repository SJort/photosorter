import io
import time
import tkinter as tk
from PIL import Image, ImageTk


def render(canvas):
    print(f"Rendering image")
    try:
        global img
        canvas.create_line(50 * 5, 0, 50 * 5, 400)
        file = open("../img/img1.JPG", "rb")
        img_mem = file.read()
        file.close()
        img = Image.open(io.BytesIO(img_mem))
        img = img.resize((250, 250), Image.ANTIALIAS)
        img = ImageTk.PhotoImage(img)
        canvas.create_image(10, 10, anchor=tk.CENTER, image=img)
        # panel = tk.Label(root, image=img)
        # panel.image = img
        # panel.pack()
    except Exception as e:
        print(f"Exception: {e}")
        # raise e


if __name__ == "__main__":
    print(f"Started image loading test.")

    def on_key(event):
        pressed_key = str(event.char)

        print(f"Pressed {pressed_key}")

        render(canvas)


    root = tk.Tk()
    root.bind("<Escape>", lambda x: root.destroy())
    root.minsize(500, 400)
    root.bind("<Key>", on_key)
    img = None
    canvas = tk.Canvas(root, bd=2, relief="solid")
    canvas.pack()
    root.mainloop()
