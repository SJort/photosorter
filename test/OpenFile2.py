import webview as w
import time


def save_file_dialog(window):
    time.sleep(5)
    result = window.create_file_dialog(w.SAVE_DIALOG, directory='/', save_filename='test.file')
    print(result)


if __name__ == '__main__':
    window = w.create_window('Save file dialog', 'https://pywebview.flowrl.com/hello')
    w.start(save_file_dialog, window)
